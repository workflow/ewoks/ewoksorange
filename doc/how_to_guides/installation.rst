Installation
============

.. tab-set::

    .. tab-item:: from pypi

        .. code-block:: bash

            pip install ewoksorange[full]

    .. tab-item:: from source code

        .. code-block:: bash
            
            git clone https://gitlab.esrf.fr/workflow/ewoks/ewoksorange.git
            # or cloning with ssh
            # git clone git@gitlab.esrf.fr:workflow/ewoks/ewoksorange.git
            cd ewoksorange
            pip install .[full]


Once install you can see :ref:`How to launch Orange canvas ?`