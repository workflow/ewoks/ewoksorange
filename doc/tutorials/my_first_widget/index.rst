My first orange widget for ewoks
================================

In this tutorial we will explain you how to create your first orange widget for an ewoks task.


.. important::

     prerequisites: we consider that you are already fluent in python and experienced with ewoks and qt concepts.


.. toctree::
    :maxdepth: 1

    starting
    testing
    input_gui_read_only
    get_input_from_gui
    display_output
    waiting_for_user_validation
