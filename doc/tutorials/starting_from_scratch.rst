.. _Starting a new project from scratch:

Starting a new project from scratch
===================================

`Orange <https://orangedatamining.com/>`_ widgets can be written and associated to the Ewoks tasks provided by an Ewoks project.

For this, the Ewoks project needs to be setup as an *Orange Add-on* project. To bootstrap your project you can use `The ewoks cookie cutter project <https://gitlab.esrf.fr/workflow/ewoksapps/ewokscookie>`_
