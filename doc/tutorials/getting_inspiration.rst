Getting inspiration from existing projects
==========================================

Several existing Ewoks projects are providing dedicated Orange bindings

* `est <https://gitlab.esrf.fr/workflow/ewoksapps/est>`_
* `ewoksfluo <https://gitlab.esrf.fr/workflow/ewoksapps/ewoksfluo>`_
* `ewoksndreg <https://gitlab.esrf.fr/workflow/ewoksapps/ewoksndreg>`_
* `ewoksxrpd <https://gitlab.esrf.fr/workflow/ewoksapps/ewoksxrpd>`_
