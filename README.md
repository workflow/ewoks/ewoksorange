# ewoksorange

_ewoksorange_ provides a desktop graphical interface for [ewoks](https://ewoks.readthedocs.io/).

## Getting started

Install the project

```bash
pip install ewoksorange[full]
```

Launch the Orange canvas:

```bash
ewoks-canvas
```

Launch the Orange canvas with the examples add-on

```bash
ewoks-canvas --with-examples
```

Open an existing workflow in the Orange canvas

```bash
ewoks-canvas /path/to/orange_wf.ows
```

For more information, see the [documentation](https://ewoksorange.readthedocs.io/): https://ewoksorange.readthedocs.io/
